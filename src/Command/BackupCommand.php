<?php

namespace Drupal\backup_restore\Command;

use Drupal\Core\Entity\Entity;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Console\Core\Command\Shared\ContainerAwareCommandTrait;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Console\Annotations\DrupalCommand;

/**
 * Class BackupCommand.
 *
 * @package Drupal\backup_restore
 *
 * @DrupalCommand (
 *     extension="backup_restore",
 *     extensionType="module"
 * )
 */
class BackupCommand extends Command {

  use ContainerAwareCommandTrait;

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('data:backup')
      ->setDescription('Backup nodes and users')
      ->addArgument(
        'path',
        InputArgument::REQUIRED,
        'Path to backup directory',
        null
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $user = User::load(1);
    user_login_finalize($user);

    $serializer = \Drupal::service('serializer');
    $file_usage = \Drupal::service('file.usage');

    $path = $input->getArgument('path');

    mkdir($path, true);
    array_map('unlink', glob($path."/**/*"));

    $types = array(
      "user" => User::class,
      "file" => File::class,
      "node" => Node::class
    );

    foreach ($types as $type => $class) {
      $dir = $path.'/'.$type;
      mkdir($dir, true);

      $ids = \Drupal::entityQuery($type)->execute();
      $entities = $class::loadMultiple($ids);

      $io->info('Backuping '.count($ids).' '.$type.'s');

      /* @var $entity Entity */
      foreach ($entities as $entity) {
        $data = $serializer->serialize($entity, 'json', array('simpleref' => true));
        if ($entity instanceof User) {
          /* @var $entity User */
          $data = substr($data, 0, -1).',"pass":"'.$entity->getPassword().'"}';
        } else if ($entity instanceof File) {
          if (count($file_usage->listUsage($entity)) === 0) {
            continue;
          }
          /* @var $entity File */
          file_unmanaged_copy($entity->getFileUri(), $dir . '/' . $entity->uuid());
        }
        file_put_contents($dir . '/' . $entity->uuid() . '.json', $data);
        $io->info('.', false);
      }
      $io->info('');
      $io->successLite('Ok');
    }

    $io->info('Backup successfully done !');
  }
}
