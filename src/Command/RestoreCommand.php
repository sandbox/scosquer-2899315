<?php

namespace Drupal\backup_restore\Command;

use Drupal\Core\Entity\Entity;
use Drupal\Core\File\FileSystem;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Console\Core\Command\Shared\ContainerAwareCommandTrait;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Console\Annotations\DrupalCommand;

/**
 * Class RestoreCommand.
 *
 * @package Drupal\backup_restore
 *
 * @DrupalCommand (
 *     extension="backup_restore",
 *     extensionType="module"
 * )
 */
class RestoreCommand extends Command {
  use ContainerAwareCommandTrait;

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('data:restore')
      ->setDescription('Restore nodes and users')
      ->addArgument(
        'path',
        InputArgument::REQUIRED,
        'Path to backup directory',
        NULL
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $serializer = \Drupal::service('serializer');
    /** @var FileSystem $filesystem */
    $filesystem = \Drupal::service('file_system');

    $path = $input->getArgument('path');

    $types = [
      "user" => User::class,
      "file" => File::class,
      "node" => Node::class,
    ];

    foreach ($types as $type => $class) {
      $io->info('Restoring ' . $type . 's');

      $ids = \Drupal::entityQuery($type)->execute();
      $entities = $class::loadMultiple($ids);

      /* @var $entity Entity */
      foreach ($entities as $entity) {
        $entity->delete();
      }

      foreach (glob($path . "/" . $type . "/*.json") as $file) {
        /* @var $entity Entity */
        $data = file_get_contents($file);
        $entity = $serializer->deserialize($data, $class, 'json', ['request_method' => 'POST']);
        $entity->enforceIsNew(TRUE);
        if ($entity instanceof User) {
          /* @var $entity User */
          $entity->get('pass')->pre_hashed = TRUE;
        }
        else {
          if ($entity instanceof File) {
            // Prepare dirname.
            $dirname = $filesystem->dirname($entity->getFileUri());
            // Prepare directory on FS.
            file_prepare_directory($dirname, FILE_CREATE_DIRECTORY);
            /* @var $entity File */
            file_unmanaged_copy($path . '/' . $type . '/' . $entity->uuid(), $entity->getFileUri(), FILE_EXISTS_REPLACE);
          }
        }
        $entity->enforceIsNew(TRUE);
        $entity->save();
        $io->info('.', FALSE);
      }
      $io->info('');
      $io->successLite('Ok');
    }
    $io->info('Restore successfully done !');
  }
}
